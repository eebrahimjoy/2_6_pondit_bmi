import 'package:bmi/utils/constants.dart';
import 'package:flutter/material.dart';

class BottomButton extends StatelessWidget {
  final Function()? onPrerssed;

  final String? title;

  BottomButton({this.onPrerssed, this.title});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPrerssed,
      child: Container(
        child: Center(
          child: Text(
            title!,
            style: kLargeButtonTextStyle,
          ),
        ),
        color: const Color(0xFFEB1555),
        margin: const EdgeInsets.only(top: 16,bottom: 16,left: 16,right: 16),
        height: kBottomContainerHeight,
        width: double.infinity,
      ),
    );
  }
}
